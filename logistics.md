# Event Logistics

## Event Details
- **Event Name:** 2023 SWH partners network sprint
  - a.k.a FAIRCORE4EOSC WP6 sprint
- **Date:** October 9th & 10th
- **Time:** 10:00 - 17:00 each day
- **Location:**  INRIA Paris Center (2 Rue Simone IFF, 75012 Paris)
- **Website:** [event repo](https://gitlab.softwareheritage.org/outreach/events/swh-partners-network-sprint)

## Agenda
[Draft agenda](agenda.md).

## Registration
- **Registration Link:** https://framaforms.org/2023-october-deposit-partners-workshop-faircore4eosc-wp6-sprint-1692714341



## Meals
### Day 1: October 9th
- **Welcome coffee:** TBC
- **Lunch:** TBC
- **Afternoon coffee:**TBC
- **Dinner:** TBC
### Day 2: October 10th
- **Welcome coffee:** TBC
- **Lunch:** TBC
- **Afternoon coffee:**TBC


## Contact Information
- **Event Organizer:** Morane Gruenpeter
  - **Email:** morane@softwareheritage.org
  - **Phone:** +33-6-50629381

## Social Media
- **Hashtag:** #Partners-Sprint
- **Social Media Handles:** 
  - [Twitter]: @SWHeritage
  - [LinkedIn]: Software Heritage

## Additional Information
