# SWH partners network sprint

## The 2023 FAIRCORE4EOSC sprint and Deposit and mirror network event
A two days working meeting for the SWH Deposit and Mirror partners.
Dates: 9-10 of October

The majority of the event will consist of small group work to assess the components. 

*update*
The mirror partners will have an online meeting on October 10th from 10:00-13:00 CEST.

### Where will the workshop take place?
- The Inria Paris research center is located at 2 rue Simone Iff.
- The closest metro stations are
    - Dugommier station - Line 6
    - Montgallet station - Line 8

### Objectives
1. Identifying the gap between the SIRS report (specifications) and the current outcome.
2. Identifying and elaborating necessary tests.
3. Documenting the components for users and developers (which is part of the BETA deliverable).

### Partners
- Present, test and finalize a first BETA for the RSAC components in the FAIRCORE4EOSC project: 
    - Deposit partners 
        - CERN with InvenioRDM
        - LZI with Dagstuhl
        - FIZ with swMath 
        - OpenAire 
        - DANS with prototype on top of DataVerse
        - IES-INRIA & CCSD with Episciences
    - Mirror partners 
        - GRNET
        - Unidue
- Share experience from existing partners:
    - Deposit partners 
        - CCSD with HAL
        - IPOL (declined invitation)
        - eLife (declined invitation)
    - Mirror partners
         - ENEA


### Logistics
[Logistics file](logistics.md)

### Agenda
The [full agenda](agenda.md) with activities and facilitation text.
In the Prints folder you will find a printable agenda and the printable activities.

### Software Heritage welcome team:
- Morane Gruenpeter
- Roberto Di Cosmo
- Marla Da Silva
- David Douard
- Benoît Chauvet
- Valentin Lorentz
- Antoine Dumont
- Jean-François Abramatic

### Onsite Participants:
- CCSD:
  - Raphaël Tournoy
  - Yannick Barborini
- CERN:
  - Manuel Alejandro De Oliveira Da Costa
- DANS:
  - Wilko Steinhoff 
- FIZ / swMath:
  - Maxence Azzouz-Thuderoz
- INRIA / IES :
    - Alain Monteil
    - Jozefina Sadowska 
    - Estelle Nivault
- LZI / Dagstuhl: 
  - Ramy-Badr Ahmed
- OpenAire:
  - Serafeim Chatzopoulos

### Online Participants:
- ENEA
- Unidue
- GRNET

## References and links
- The SIRS report diagrams: https://hedgedoc.softwareheritage.org/fc4e-sirs-sequence-diagrams
- Tool to export mermaid diagrams: https://mermaid.live/ (swh:1:dir:ff98e14f160a8758c3b58e7d3f06dfd5df7d254c;origin=https://github.com/mermaid-js/mermaid-live-editor;visit=swh:1:snp:ce2fd45f30af4f7a9d71bbc0a3ff53f7203cebc6;anchor=swh:1:rev:83382901cd7e15414b6f18b48b7dd9c4775f3a21)
- Tests with HAL: https://hedgedoc.softwareheritage.org/EL4a-5BERT6wCY92lLfSNA?both
- RSMD guidelines: https://doi.org/10.5281/zenodo.8199104
