# Event agenda 

[TOC] 

## Possible topics to address in deposit sprint
1. Demonstrtaion by each infrastructure, showing the work done on the software submission interface and integrartion with SWH
2. The interface and UX | The users and personas using the interface
3. The workflow of depositing a software artifact
4. The result in SWH: what is accessible and what isn't
5. Tests to ensure that the components are working with all test scenarios
6. Exports: which formats are available
7. Integration with other service in the landscape (ORCID, CodeMeta, schema.org)
8. iFrame: browsing the code in the metadata record
9. Debugging: finding technical solution for errors
10. User docs: how to use and hands on tutorials
11. Communication and outreach (too early?) - propose a com plan
12. Curation workflows: how is the curator and what is the interface proposed for this actor (preparing for D6.1- due in September 2024)
13. Relationship types between Research outputs

--------------------- 
## Day 1 - Deposit Sprint

**9:30-10:00 - Sprint launch: welcome coffee**

### 10:00 Welcome & Sprint goals *(20min)*
👤 Morane 

:pencil: Slides

- The deposit feature
- The sprint goals
- Rules
 

### 10:20 Getting to know each other *(10min)*
👤 Morane 
 
- Classify by:
    - Names
    - Distance from INRIA
    - Organization 
    - Platforme name / feature name

### 10:30 Session 1: Revisiting the SPECS *(60 min)*
👤 Morane 

:pencil: post-its in 3 colors, printed SPECS v2.0 

- Each small group will view one or two demo and revisit the SPECS.
- Goal:
    - identify gaps between the specs and the demo (do not try to answer how and when, just identify the gap exists)
    - collect questions that we might be able to answer during the sprint

- On the post-its:
    - color X: Gap = Task (gaps that can become tasks)
    - color Y: Gap = enhancement (gaps that are additions for the SPECS)
    - color Z: Questions (any question, it can be SWH related, user related, developer related)

- One person to report back after the break

Text for SPECS tasks activity
With your team you have 1 hour minutes to demonstrate each component in your team and revisit the SPECS.
For your convenience you will find in this envelope the following:
- the SPECS for the connectors (2 per connector)
- the MS31 report with the september status
- the WP6 reporting instructions
Your mission is to use the post-its that are available for the following:
- Yellow = Tasks (features and elements that are not yet available)
- Pink = Features that are available in the demo
- Blue = Questions (it can be any question, user related, tech related, SWH related)

In the report back we will put all the post-its on the "Deposit Puzzle wall"
And one volunteer will share with everyone the surprises from this first SPECS tasks session

### 11:30 Break *(15 min)*



### 11:45  Identify tasks and challenges *(45min)*
👤 Morane 

:pencil: Craft paper on a wall/board
- Each group presenter to share what happened during the SPECS session
- All to add the post-its to the "deposit puzzle wall"


### 12:45 Lunch break *(1h30)*


### 14:00 Warm up *(15 min)*
👤 Morane 

 - Pac-man?


### 14:15 Session 2 - Test & deployment *(60 min)*
👤 Benoît?

:pencil: print test templates, print empty checklist for deployment

- Each group will get a list of possible tests (identified in WP6) and a template
- In small groups, we are going to choose relevant tests
    - test the demo
        - if fails, make the code work or identify tasks to do
        - if passes, see if we can rewrite better code
    - do another test
    - write results on test templates 
        - transform fails into issues/questions
        - add to the "deposit puzzle wall"

- **Bonus:** BETA RELEASE deployment. Each group will identify the steps towards the deployment of the subcomponent in/with staging.

### 15:15 Break *(15min)*

### 15:30 Session 3 - free form work *(1h)*

👤 Morane

:pencil: all to bring laptops, extensions
- in groups or alone, identify one aspect to work on
- possibilities:
    - update the SPECS
    - implement functionality
    - crosswalk a metadata mapping (to CodeMeta)
    - write issues to be implemented after the sprint
    - write test cases / implement tests
    - write / improve documentation

### 16:30  Plenary *(25 min)*

👤 Morane

- Share takeaways with the full group
- Comments, surprises, obvious miss
- What will be the best usage of the 2nd day for you?

### 16:55 Closing for the day *(5 min)*



## Day 2 - Deposit Sprint
**9:30-10:00 -  welcome coffee** on ground floor

### 10:00 Warming up *(15 min)*
👤 Morane

Walk to occupy the space. Break with someone close to you:
- How do you feel this morning?
- Something you enjoyed yesterday?
- Something you are looking for today?

### 10:15  Session 4 - documentation *(1h)*
👤 Morane

:pencil: 5 giga post-its, sharpies, building blocks from SWHAP workshop

- Who are the platform's users?
- Who are the readers? (choose a reader)
- What do they need to know?
- What materials do they need (videos, diagrams, written tutorials, etc.)
- Activity
    - On the Paper board design the documentation needed to the people that are using the components
    - You can visit existing documentation and build upon it


### 11:15 Break *(15 min)*

###  11:30  Session 5 - free form work *(45min)*
👤 Morane

:pencil: laptop, extensions
- in groups or alone, identify one aspect to work on
- possibilities:
    - update the SPECS
    - implement functionality
    - crosswalk a metadata mapping (to CodeMeta)
    - write issues to be implemented after the sprint
    - write test cases / implement tests
    - write / improve documentation
    - Review if the demo can be used with the RSMD guidelines (print RSMD checklists)


### 12:15  Group picture *(10 min)*

⌚ 12:15  
- team photo with polo t-shirts

⌚ 12:30
- all event participants photo

### 12:45 Lunch *(1h15)*


### 14:00  Warming up *(15 min)*
 
👤 Morane

- Counting together

### 14:15 Session 6: Final hack session *(60min)*

👤 Morane
:pencil: laptop
- in groups or alone, identify one aspect to work on
- possibilities:
    - update the SPECS
    - implement functionality
    - crosswalk a metadata mapping (to CodeMeta)
    - write issues to be implemented after the sprint
    - write test cases / implement tests
    - write / improve documentation
    - Review if the demo can be used with the RSMD guidelines (print RSMD checklists)

### 15:15 Break *(15 min)*

### 15:30 Sharing results *(60 min)*

👤 Morane

- Each subcomponent lead to share the outcomes from the sprint and from the sessions
    - without a laptop!
    - share input recieved, challenges/limitations identified
    - share added value
    - share concrete outcomes (without a demo) 
    - MAX 10 minutes per platforme
    - Demo will be scheduled as webinars after the BETA-RELEASE

### 16:30 Next steps *(20 min)*

👤 Morane

:pencil: Post-its

What are the next steps you would like to see? Which ones would you like to actually do? → using post-its
- red = things you would like to see happen
- green = things you would like to do

Read out loud in the circle and post on the wall


-------------------------

## Mirror Network closed meeting online
⌚ 10:00  - 12:15

👤 David

- Presentation of the partcipants around the table (10 min)
- Presentation (by David/Robero): of the Software Heritage Mirror goals (20 min)
- Presentation of the Software Stack of a mirror: by David (20 min)
- Open questions (25 min)


⌚ 11:15 break

⌚ 11:30 - 12:15 The support the network needs 
- We are building a Mirror Network: how does this collaboration looks like?
- Identifying tools to make the mirror network work?
- Identifying how to improve the SWH mirror documentation? 



⌚ 12:00 end
